local src = require("lang/"..about.settings.lang.."/true-7")
script = {
	[1] = { "set_page", { name = "Dark Alley 6", image = "cg-1/6" } },
	[2] = { "play_cancel" },
	[3] = { "print", { msg = src[1], mode = 0 } },
	[4]	= { "play", { mode = 1, name = "fatal_scene", loop = true } },
	[5] = { "set_page", { name = "Dark Alley 7", image = "cg-1/7" } } ,	
	[6] = { "print", { msg = src[2], mode = 1 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 1 }, { [1] = { text = src[3], name = "true-8" }, [2] = { text = src[4], name = "true-8" }}},
}
return script