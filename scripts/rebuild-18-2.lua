local src = require("lang/"..about.settings.lang.."/rebuild-18-2")
script = {
	[1] = { "set_page", { name = "Heavy Thoughts 3", image = "cg-2/17" } },
	[2] = { "fade_text", { dir = "in", time = 0.5 } },
	[3] = { "clear" },
	[4] = { "set_page", { name = "Heavy Thoughts 4", image = "cg-2/18-2" }, { mode = "flip", dir = "left", time = 1 } },
	[5] = { "unlock", { type = 1, id = 6 } },
	[6] = { "fade_text", { dir = "out", time = 1 } },
	[7] = { "print", { msg = src[1], mode = 0 } },
	[8] = { "wait", { time = 0.5 } },
	[9] = { "input", { type = 1 }, { [1] = { text = src[2], name = "mos-1" }, [2] = { text = src[3], name = "rebuild-19-2"} } },
}
return script