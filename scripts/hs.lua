local src = require("lang/"..about.settings.lang.."/hs")
script = {
	[1] = { "play", { mode = 0, id = 16 } },
	[2] = { "fill", { mode = "in", time = 1.5, r = 255, g = 255, b = 255 } },
	[3] = { "set_page", { name = "Halloween - Intro", image = "cg-4/0" } },
	[4] = { "fade_text", { instant = true, dir = "in" } },
	[5] = { "wait", { time = 0.5 } },
	[6] = { "fill", { mode = "out", time = 3, r = 255, g = 255, b = 255 } },
	[7]	= { "play", { mode = 1, name = "yuzure_nai_omoi_cut", loop = false } },
	[8] = { "set_page", { name = "Halloween - Intro", image = "cg-4/1" }, { mode = "fade", time = 0.5 }},
	[9] = { "set_page", { name = "Halloween - Intro", image = "cg-4/2" }, { mode = "fade", time = 0.1 }},
	[10] = { "set_page", { name = "Halloween - Intro", image = "cg-4/3" }, { mode = "fade", time = 0.2 }},
	[11] = { "set_page", { name = "Halloween - Intro", image = "cg-4/4" }, { mode = "fade", time = 0.2 }},
	[12] = { "set_page", { name = "Halloween - Intro", image = "cg-4/5" }, { mode = "fade", time = 1 }},
	[13] = { "play", { mode = 0, id = 24 } },
	[14] = { "set_page", { name = "Halloween - Intro", image = "cg-4/6" }, { mode = "fade", time = 1 }},
	[15] = { "set_page", { name = "Halloween - Intro", image = "cg-4/1" }, { mode = "fade", time = 0.5 }},
	[16] = { "wait", { time = 0.5 } },
	[17] = { "set_page", { name = "Halloween - Intro", image = "cg-4/2" }, { mode = "fade", time = 0.1 }},
	[18] = { "set_page", { name = "Halloween - Intro", image = "cg-4/3" }, { mode = "fade", time = 0.2 }},
	[19] = { "set_page", { name = "Halloween - Intro", image = "cg-4/4" }, { mode = "fade", time = 0.2 }},
	[20] = { "play", { mode = 0, id = 23 } },
	[21] = { "set_page", { name = "Halloween - Intro", image = "cg-4/5" }, { mode = "fade", time = 1 }},
	[22] = { "set_page", { name = "Halloween - Intro", image = "cg-4/11" }, { mode = "fade", time = 1 }},
	[23] = { "set_page", { name = "Halloween - Intro", image = "cg-4/12" }, { mode = "fade", time = 0.5 }},
	[24] = { "fill", { instant = true, mode = "in" } },
	[25] = { "fill", { instant = true, mode = "out" } },
	[26] = { "set_page", { name = "Halloween - Intro", image = "cg-4/11" }, { mode = "fade", time = 0.1 }},
	[27] = { "set_page", { name = "Halloween - Intro", image = "cg-4/12" }, { mode = "fade", time = 0.5 }},
	[28] = { "set_page", { name = "Halloween - Intro", image = "cg-4/11" }, { mode = "fade", time = 0.1 }},
	[29] = { "play", { mode = 0, id = 6 } },
	[30] = { "fill", { mode = "in", time = 0.2, r = 255, g = 255, b = 255 } },
	[31] = { "set_page", { name = "Halloween - Intro", image = "cg-4/16" } },
	[32] = { "fill", { mode = "out", time = 0.2, r = 255, g = 255, b = 255 } },
	[33] = { "fill", { mode = "in", time = 0.2, r = 255, g = 255, b = 255 } },
	[34] = { "add_layer", { name = "grin1", img = "grin1" }, color = { r = 255, g = 255, b = 255, alpha = 0 } },
	[35] = { "add_layer", { name = "grin2", img = "grin2" }, color = { r = 255, g = 255, b = 255, alpha = 0 } },
	[36] = { "set_layer", "grin1", { color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[37] = { "set_layer", "grin2", { color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[38] = { "set_page", { name = "Halloween - Intro", image = "cg-4/17" } },
	[39] = { "fill", { mode = "out", time = 0.2, r = 255, g = 255, b = 255 } },
	[40] = { "set_page", { name = "Halloween - Intro", image = "cg-4/18" }, { mode = "fade", time = 1 }},
	[41] = { "add_layer", { name = "filter", img = "videofilter" } },
	[42] = { "wait", { time = 0.2 } },
	[43] = { "remove_layer", "filter" },
	[44] = { "set_page", { name = "Halloween - Intro", image = "cg-4/19" }, { mode = "fade", time = 0.2 }},
	[45] = { "set_page", { name = "Halloween - Intro", image = "cg-4/20" }, { mode = "fade", time = 0.2 }},
	[46] = { "set_page", { name = "Halloween - Out of Place", image = "static/blank" }, { mode = "fade", time = 1 }},
	[47] = { "set_align", "center" },
	[48] = { "set_font", 2 },
	[49] = { "print", { msg = src[1], mode = 0 } },
	[50] = { "wait", { time = 1 } },
	[51] = { "fade_text", { dir = "out", time = 1 } },
	[52] = { "wait", { time = 2 } },
	[53] = { "fade_text", { dir = "in", time = 1 } },
	[54] = { "play", { mode = 1, name = "se221", loop = true } },
	[55] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/22" }, { mode = "fade", time = 3 }},
	[56] = { "clear" },
	[57] = { "set_font", 1 },
	[58] = { "print", { msg = src[2], mode = 0 } },
	[59] = { "fade_text", { dir = "out", time = 1 } },
	[60] = { "wait", { time = 2 } },
	[61] = { "fade_text", { dir = "in", time = 1 } },
	[62] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/23" }, { mode = "fade", time = 0.5 }},
	[63] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/24" }, { mode = "fade", time = 0.2 }},
	[64] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/25" }, { mode = "fade", time = 0.5 }},
	[65] = { "clear" },
	[66] = { "set_align", "left" },
	[67] = { "set_font", 0 },
	[68] = { "fade_text", { dir = "out", time = 1 } },
	[69] = { "print", { msg = src[3], mode = 0 } },
	[70] = { "input", { type = 0 } },
	[71] = { "wait", { time = 0.5 } },
	[72] = { "print", { msg = src[4], mode = 0 } },
	[73] = { "input", { type = 0 } },
	[74] = { "wait", { time = 0.5 } },
	[75] = { "fade_text", { dir = "in", time = 1 } },
	[76] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/26" }, { mode = "fade", time = 0.5 }},
	[77] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/27" }, { mode = "fade", time = 0.5 }},
	[78] = { "clear" },
	[79] = { "fade_text", { dir = "out", time = 1 } },
	[80] = { "print", { msg = src[5], mode = 0 } },
	[81] = { "input", { type = 0 } },
	[82] = { "wait", { time = 0.5 } },
	[83] = { "play", { mode = 0, id = 20 } },
	[84] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/28" }, { mode = "fade", time = 0.3 }},
	[85] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/27" }, { mode = "fade", time = 0.3 }},
	[86] = { "wait", { time = 0.5 } },
	[87] = { "print", { msg = src[6], mode = 1 } },
	[88] = { "input", { type = 0 } },	
	[89] = { "wait", { time = 0.5 } },
	[90] = { "fade_text", { dir = "in", time = 1 } },
	[91] = { "clear" },
	[92] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/29" }, { mode = "flip", dir = "down", time = 1 } },
	[93] = { "wait", { time = 0.5 } },
	[94] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/30" }, { mode = "flip", dir = "left", time = 1 } },
	[95] = { "wait", { time = 0.5 } },
	[96] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/31" }, { mode = "flip", dir = "left", time = 1 } },
	[97] = { "wait", { time = 0.5 } },
	[98] = { "set_page", { name = "Halloween - Out of Place", image = "cg-4/32" }, { mode = "flip", dir = "up", time = 1 } },
	[99] = { "wait", { time = 0.5 } },
	[100] = { "set_page", { name = "Halloween - Dark Alley", image = "cg-4/33" }, { mode = "flip", dir = "down", time = 1 } },
	[101] = { "fade_text", { dir = "out", time = 1 } },
	[102] = { "print", { msg = src[7], mode = 0 } },
	[103] = { "input", { type = 0 } },
	[104] = { "wait", { time = 0.5 } },
	[105] = { "play_cancel" },
	[106] = { "fade_text", { dir = "in", time = 0.5 } },
	[107] = { "wait", { time = 0.5 } },
	[108] = { "fade_layer", "grin1", { dir = "out", time = 1 } },
	[109] = { "wait", { time = 0.2 } },
	[110] = { "play", { mode = 0, id = 25 } },
	[111] = { "fade_layer", "grin2", { dir = "out", time = 0.5 } },
	[112] = { "remove_layer", "grin1" },
	[113] = { "wait", { time = 1 } },
	[114] = { "play", { mode = 1, name = "strange_feeling", loop = true } },
	[115] = { "fade_layer", "grin2", { dir = "in", time = 1 } },
	[116] = { "remove_layer", "grin2" },
	[117] = { "add_layer", { name = "chiyo", img = "blue-chiyo", color = { r = 255, g = 255, b = 255, alpha = 0 } } },
	[118] = { "set_layer", "chiyo", { y = 600 } },
	[119] = { "fade_layer", "chiyo", { dir = "out", time = 1 } },
	[120] = { "clear" },
	[121] = { "set_db_style", 2 },
	[122] = { "fade_text", { dir = "out", time = 1 } },
	[123] = { "print", { msg = src[8], mode = 0 } },
	[124] = { "wait", { time = 0.5 } },
	[125] = { "input", { type = 1 }, { [1] = { text = src[9], name = "hs-2" } } },
	
--	[26] = { "fill", { instant = true, mode = "in" } },
	
}
return script