local src = require("lang/"..about.settings.lang.."/true-26")
script = {
	[1] = { "set_page", { name = "At School 1", image = "cg-1/26" }} ,
	[2] = { "play", { mode = 1, name = "school_days", loop = true } },
	[3] = { "fill", { mode = "out", time = 3 } },
	[4] = { "unlock", { type = 2, id = 4 } },
	[5] = { "fade_text", { dir = "out", time = 1 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 0 } } ,
	[9] = { "fade_text", { dir = "in", time = 0.5 } },
	[10] = { "clear" },
	[11] = { "read", { name = "true-27" } },
}
return script