local src = require("lang/"..about.settings.lang.."/rebuild-35")
script = {
	[1] = { "set_page", { name = "Nisekoi", image = "cg-2/34" }},
	[2] = { "fade_text", { dir = "in", time = 1 } },
	[3] = { "set_page", { name = "Nisekoi", image = "cg-2/35" }, { mode = "fade", time = 1 }},
	[4] = { "fade_text", { dir = "out", time = 1 } },
	[5] = { "set_color", { 120, 153, 34 } },
	[6] = { "print", { msg = src[1], mode = 0 } },
	[7] = { "wait", { time = 0.5 } },
	[8] = { "input", { type = 1 }, { [1] = { text = src[2], name = "rebuild-36-37" } } },	
}	
return script