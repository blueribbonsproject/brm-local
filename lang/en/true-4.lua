local src = {}
src[1] = '"Haha...ha, how\'d that get there? It\'s gone now.'
src[2] = 'So can I come over tonight Nozaki-kun?"'
src[3] = "Chiyo, I have a secret. I'm not the original Nozaki-kun, I'm just someone who's inserting onto Nozaki-kun. I'm not really Nozaki-kun, I'm just pretending to be him. Don't you get it Chiyo? I couldn't bear to hide it any longer. The real Nozaki-kun is long gone, locked up producing manga all day and night."
src[4] = "D-do you have a sister or something Chiyo?"
src[5] = "Is that a ghost?!"
return src