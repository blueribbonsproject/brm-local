local src = {}
src[1] = "When I got home, there she was."
src[2] = '"Nozaki-kun, you did the right thing!\nEverything was a delusion created by her.\nThere is only one Sakura Chiyo for Umetarou Nozaki!"'
src[3] = '"Nozaki, safe? Red Chiyo, Purple Illusion."'
src[4] = '(bread making sounds)'
src[5] = '"I love you, Chaika!"'
src[6] = '"More than any other Chaika in the world."'
src[7] = '"Let\'s find your father\'s remains together."'
src[8] = '"Me and you."'
src[9] = '"Since when you were under the impression that this thread happened?"'
return src