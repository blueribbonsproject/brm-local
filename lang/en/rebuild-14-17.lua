local src = {}
src[1] = '"Kill other Chiyos. Obtain Ribbon.\nRevive original. Only way!"'
src[2] = 'Will I be the same person after killing the rest of the Chiyos out there?'
src[3] = 'There\'s a different one everywhere I go.'
src[4] = '"What makes one Sakura different from another? A Chiyo by any other color...is just as cute."\nWith this heavy thought in mind, I\'m going to kill...'
src[5] = "Yellow"
src[6] = "Green"
src[7] = "Purple"
return src