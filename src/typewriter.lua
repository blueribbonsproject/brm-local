--// Typewriter prints text and controls overlay/text fading
--// Accepts instructions from pager.

local class = require "libs/hump/class"
local timer = require "libs/hump/timer"

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc = game.cx, game.cy, game.xc
local ssub, slen

if web == true then
	slen = string.len
	ssub = string.sub
else
	slen = string.utf8len
	ssub = string.utf8sub
end

typewriter = class {}

function typewriter:init(input)
	
	self.state = 0	--// 0 - free, 1 - busy
	self.color = {	r = 0, g = 0, b = 0, alpha = 127	}
	self.textcolor = {	r = 255, g = 255, b = 255, alpha = 255	}
	self.message = ""		--"Hey! Hey! Nozaki-kun!\nI finished the draft for the dialogue you asked me for :) "
	if web == true then self.message = " " end
	self.buffer = " "	
	self.timer = timer.new()
	self.align = "left"
	self.font = "regular"
	self.dbstyle = 1
	self.dbstyles = {}
	self.dbdecor = love.graphics.newImage("graphics/static/quote.png")
	self.input = input
	self.pointer = " "
	
	self.dbstyles[1] = {	--// Fate-like message box (takes the whole screen)
		x = cx + 75 * sc,
		y = 60 * sc,
		w = 800 * sc,
		h = 600 * sc,
		tx = cx + 105 * sc,
		ty = cy + 90 * sc,
		tw = 740 * sc,
	}
	
	self.dbstyles[2] = {	--// small message box in the bottom of the screen, similar to many shitty dating sims
		x = cx + 24 * sc,
		y = 484 * sc ,
		w = 902 * sc,
		h = 212 * sc,
		tx = cx + 64 * sc,
		ty = cy + 508 * sc,
		tw = 842 * sc,
	}
	
end

function typewriter:update(dt)
	self.timer:update(dt)
	if self.input.state == 1 and self.input.type == 0 and self.state == 0 then
		self.pointer = " <...>"
	else
		self.pointer = " "
	end
end

function typewriter:draw()
	if self.color.alpha ~= 0 then
		love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.alpha)
		love.graphics.rectangle('fill', self.dbstyles[self.dbstyle].x, self.dbstyles[self.dbstyle].y, self.dbstyles[self.dbstyle].w, self.dbstyles[self.dbstyle].h)
		
		love.graphics.setColor(255, 255, 255, self.textcolor.alpha)
		if self.dbstyle == 2 then
			love.graphics.draw(self.dbdecor, cx + 39 * sc, 499 * sc, 0, sc, sc)
			love.graphics.draw(self.dbdecor, xc - 39 * sc, sH - 39 * sc, 3.14, sc, sc)
		end
		
		love.graphics.setColor(self.textcolor.r, self.textcolor.g, self.textcolor.b, self.textcolor.alpha)
		love.graphics.setFont(game.fonts[self.font])
		love.graphics.printf(self.buffer..self.pointer, self.dbstyles[self.dbstyle].tx, self.dbstyles[self.dbstyle].ty, self.dbstyles[self.dbstyle].tw, self.align)	
	end	
end

function typewriter:hide(time, onComplete, instant)
	local oc = onComplete or function()	self.color.alpha = 0	self.textcolor.alpha = 0	self.state = 0	end
	if not instant then
		local t = time or 2	
		self.timer:tween(t,  self.color, {	alpha = 0	}, "linear", oc)
		self.timer:tween(t,  self.textcolor, {	alpha = 0	}, "linear", oc)
	else
		self.color.alpha = 0
		self.textcolor.alpha = 0
		oc()
	end
end

function typewriter:show(time, onComplete, instant)
	
	local oc = onComplete or function()	self.color.aplha = 255	self.textcolor.alpha = 255	self.state = 0	end
	if not instant then
		local t = time or 2		
		self.timer:tween(t,  self.color, {	alpha = 127	}, "linear", oc)
		self.timer:tween(t,  self.textcolor, {	alpha = 255	}, "linear", oc)
	else
		self.color.alpha = 127
		self.textcolor.alpha = 255
		oc()
	end
end

function typewriter:write(msg, mode, instant)
	local m = mode or 0		--// 0 - rewind buffer, 1 - add new lines
	local switch = {
		[0]	=	function()	self.message = msg	self.buffer = "" if web == true then self.buffer = " " end	end,
		[1]	=	function()	self.message = self.message.."\n"..msg	end,
	}
	switch[m]()	
	if not instant then
		self.timer:addPeriodic(about.settings.textspd, function()
			self.buffer = ssub(self.message, 1, slen(self.buffer) + 1)
			if slen(self.buffer) >= slen(self.message) then
				 self.state = 0
			end
		end, slen(self.message))
	else
		self.buffer = self.message
		self.state = 0
	end
end

function typewriter:clear()
	if web == true then
		self.message = " "
		self.buffer = " "
	else
		self.message = ""
		self.buffer = ""
	end
end

function typewriter:setColor(p)
	if p and #p > 0 then
		self.textcolor.r = p[1] or 255
		self.textcolor.g = p[2] or 255
		self.textcolor.b = p[3] or 255
		if p[4] then self.textcolor.alpha = p[4] end	--// set alpha only in case it was explicitly stated
	else
		self.textcolor.r, self.textcolor.g, self.textcolor.b = 255, 255, 255
	end
end

function typewriter:setAlign(align)
	self.align = align or "left"
end

function typewriter:setFont(f)
	local font = f or 0
	local switch = {
		[0] = function()	self.font = "regular"	end,
		[1] = function()	self.font = "large"	end,
		[2] = function()	self.font = "small"	end,
	}
	switch[font]()
end