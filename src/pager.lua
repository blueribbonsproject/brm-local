--// Page controller class. Handles page switching, transitions and effects by reading the script file and sending corresponding instructions to page instances.
--// Typewrites text accordingly to a script file.
--// Also contols background music, sound effects and user input.

require "src/page"
require "src/layer"
require "src/typewriter"
require "src/input"
local json = require("libs/dkjson")
local class = require "libs/hump/class"
local timer = require "libs/hump/timer"
local tremove = table.remove

local sW, sH, sc = game.sW, game.sH, game.sc
--local mfloor = math.floor
local lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)

pager = class {}

function pager:init()
	self.blankpage = page({
		name = "blank",
		image = "static/blank",
		color = { r = 0, g = 0, b = 0, alpha = 255 },
	})
	self.currentpage, self.nextpage = self.blankpage, self.blankpage
	self.currentimg = "static/blank"
	self.current_scene = "blank"
	self.currentbgm = "none"
	self.timer = timer.new()
	self.dbstyle = 1	--// dialogue box style
	self.input = input() 		--// player input controller
	self.typewriter = typewriter(self.input)
	self.enabled = false
	self.paused = false
	self.script = {}
	self.current_line = 1
	self.state = 0
	self.color = { r = 0, g = 0, b = 0, alpha = 0 }
	self.notify = false
	self.notify_text = ""
	self.notify_color = { game.activeColor[1], game.activeColor[2], game.activeColor[3] }
	self.notify_alpha = 0
	self.notify_sound = game.sfx[11]
	self.layers = {}
	self.layers_state = 0
	
	--// Lookup table for pager:read(). Critical instructions get copied to shadow image.
	self.cmd = {		
		["set_page"] 	=	function(p, tr)	self.state = 1				self:setPage(p, tr)	end,
		["fade_page"]	=	function(p)		self.currentpage.state = 1	self:fadePage(p)	end,
		["fade_text"]	=	function(p)		self.typewriter.state = 1	self:fadeText(p)	end,
		["fade_all"]	=	function(p)		self.currentpage.state = 1	self.typewriter.state = 1	
											self:fadeText(p)			self:fadePage(p)	end,
		["zoom_page"]	=	function(p)		self.currentpage.state = 1	self:zoomPage(p)	end,
		["print"]		=	function(p)		self.typewriter.state = 1	self:print(p)		end,
		["input"]		=	function(p, m)	self.input.state = 1		self:handleinput(p, m) 	end,
		["play"]		=	function(p)		self:play(p)				end,
		["play_cancel"]	=	function()		self:cancelPlayback()		end,
		["wait"]		=	function(p)		self.state = 1				self:wait(p)		end,
		["fill"]		=	function(p)		self.state = 1				self:fill(p)		end,
		["read"]		=	function(p)		self:open(p.name or 1)
											self:read()
										end,
		["exec"] 		=	function(p)		local file = require(p)		file.init()			end,	--// executes an external script
		["clear"]		= 	function()		self.typewriter:clear()							end,
		["finish"]		=	function(p)		GS.switch(require("states/credit_roll"), p)		end,	--// jump to the credit roll when the journey is over.
		["set_color"]	=	function(p)		self.typewriter:setColor(p)						end,
		["set_align"]	=	function(p)		self.typewriter:setAlign(p)						end,	--// "left", "right" ot "center"
		["rotate"]		=	function(p)		self.currentpage:rotate(p)						end,
		["unlock"]		=	function(p)		self:unlockAchievement(p)						end,
		["set_db_style"]=	function(p)		local dbs = p or 1
											self.typewriter.dbstyle, self.input.dbstyle = dbs, dbs
									end,
		["set_font"]	=	function(p)		self.typewriter:setFont(p)						end,
		["redirect"]	=	function(p)		if web == true then										--// web-specific stuff
												domapi.window.location=p
											end
										end,
		["add_layer"]	=	function(p)		self:addLayer(p)								end,	--// adds new sprite layer
		["remove_layer"]=	function(p)		self:removeLayer(p)								end,	--// removes it
		["set_layer"]	=	function(p, m)	self:setLayer(p, m)								end,	--// applies all kinds of transformations to sprite layer
		["fade_layer"]	=	function(p, m)	self:fadeLayer(p, m)							end,
		["quit"]		=	function()		TEsound.playLooping("sound/bgm/kimi_no_iru_machi.ogg", "bgm")
											GS.switch(require("states/mainmenu"))			end,	--// just quit to menu in case there's no credit roll prepared
	}
end

function pager:update(dt)
	if self.paused == false then
		self.currentpage:update(dt)
		self.nextpage:update(dt)
		for i = 1, #self.layers, 1 do
			self.layers[i]:update(dt)
		end
		self.typewriter:update(dt)
		self.input:update(dt)
		self.timer:update(dt)			
		if self.enabled then
			local layers_state = 0
			for k, v in pairs(self.layers) do
				if v.state == 1 then layers_state = 1 end
			end
			self.layers_state = layers_state
			if self.state == 0 and self.typewriter.state == 0 and self.currentpage.state == 0 and self.input.state == 0 and self.layers_state == 0 then		--// if everything is ready, read the next line
				self:read()
			end
		end
	end
end

function pager:draw()
	if self.paused == false then
		self.nextpage:draw()
		self.currentpage:draw()
		for i = 1, #self.layers, 1 do
			self.layers[i]:draw()
		end
		self.typewriter:draw()
		self.input:draw()
		love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.alpha)
		love.graphics.rectangle('fill', 0, 0, sW, sH)
		if self.notify == true then
			love.graphics.setFont(game.fonts["small"])
			love.graphics.setColor(self.notify_color[1], self.notify_color[2], self.notify_color[3], self.notify_alpha)
			love.graphics.printf(self.notify_text, 0, 32 * sc, sW, "center")
		end
	end
end

function pager:pause()
	self.paused = true
end

function pager:setPage(p, tr)		--// { page metadata }, transition = { mode, time, dir (if mode = "flip") }
	self.nextpage = page(p)
	if not p.instant then
		if tr then
			local m = tr.mode or "fade"
			local t = tr.time or 3
			local switch = {
				["fade"] = function()	
					self.currentpage:hide(t, function()
						self.currentpage = self.nextpage
						self.nextpage = self.blankpage
						self.state = 0
					end)
					
				end,
				["flip"] = function()
					local d = tr.dir or "left"
					self.currentpage:move(t, d)
					self.nextpage:move(t, d, true, function()
						self.currentpage = self.nextpage
						self.nextpage = self.blankpage
						self.state = 0	
					end)	
				end,
			}
			switch[m]()
		else
			self.currentpage = self.nextpage
			self.nextpage = self.blankpage
			self.currentimg = p.image
			self.current_scene = p.name
			self.state = 0
		end
	else
		self.currentpage = self.nextpage
		self.nextpage = self.blankpage
		self.currentimg = p.image
		self.current_scene = p.name
		self.state = 0
	end
	about.session.cslot.image = self.currentimg
	about.session.cslot.scene = self.currentpage.name
end

function pager:fadeText(p)		--// { dir, time }
	local d = p.dir or "in"
	local t = p.time or 2
	local switch = {
		["in"] 		= function()	self.typewriter:hide(t, _, p.instant)	end,
		["out"] 	= function()	self.typewriter:show(t, _, p.instant)	end,
	}
	switch[d]()
end

function pager:fadePage(p)		--// { dir, time }
	local d = p.dir or "in"
	local t = p.time or 2
	local switch = {
		["in"] 		= function()	self.currentpage:hide(t)	end,
		["out"] 	= function()	self.currentpage:show(t)	end,
	}
	switch[d]()
end

function pager:zoomPage(p)	--// { dir, coords, amount, time }
	local d = p.dir or "in"
	local c = p.coords --or { x = 576, y = 432 }
--	print(c.x, c.y)
	local t = p.time or 2
	local a = p.amount or 2		--// x2 zoom
	local switch = {
		["in"] 		= function()	self.currentpage:zoomIn(t, c, a, _, p.instant)		end,
		["out"] 	= function()	self.currentpage:zoomOut(t, c, a, _, p.instant)	end,
	}
	switch[d]()
end

function pager:print(p)		--// { msg, mode }
	self.typewriter:write(p.msg, p.mode, p.instant)
end

function pager:handleinput(p, m)	--// { type, labels = {} }
	local correction = { ["en"] = 50, ["ru"] = 100 }
	local lines = #self.typewriter.message/correction[about.settings.lang]
	if p.type == 0	then		--// 0 - single, 1 - multiple
		self.input:single()
	else						 --// multiple only tells the input controller how many dialog options to make and what file to load when the option is chosen
		self.input:multiple(m, lines)	--// input needs to know how many lines are taken by text to place options correctly
	end
end

function pager:open(s, c)
	self.current_line = c or 1
	about.session.cslot.script = s
	about.session.cslot.line = c
	self.script = require("scripts/"..s)
	
	
end

function pager:read()		--// reads the script one line at a time and translates it to its own instructions
	local cmd = self.cmd
	local c = self.current_line
	local s = self.script
	self.enabled = true
	self.state = 0
	local check = {
		["set_page"] = 0,
		["zoom_page"] = 0,
		["print"] = 0,
--		["input"] = 0,
		["fill"] = 0,
		["play"] = 0,
--		["play_cancel"] = 0,
		["fade_text"] = 0,
		["rotate"] = 0,
		["set_db_style"] = 0,
		["set_font"] = 0,
		["add_layer"] = 0,
		["set_layer"] = 0,
		["remove_layer"] = 0,
	}
	if self.current_line <= #self.script then
		if check[s[c][1]] then
			if s[c][1] ~= "play" then
				about.session.cslot.shadow[s[c][1]] = s[c]
			else
				if s[c][2].mode ~= 0 then
					about.session.cslot.shadow[s[c][1]] = s[c]
				end
			end
		end
		cmd[s[c][1]](s[c][2], s[c][3])
		if s[c][1] == "read" then
		else
			self.current_line = self.current_line + 1
		end
	else
		self.enabled = false
	end	
end

function pager:play(p)		--// { mode, name_or_id, loop }
	local m = p.mode or 1	--// modes: 0 - sfx, 1 - bgm
	local l = p.loop or true
	if about.session.cslot.bgm then	about.session.cslot.bgm.name = "none"	end
	
	local switch = {
		[0] = function()	TEsound.play(game.sfx[p.id], _, 0.6)	end,
		[1] = function()	about.session.cslot.bgm = { name = p.name, loop = l }
							TEsound.stop("bgm")
							if p.loop then
								TEsound.playLooping("sound/bgm/"..p.name..".ogg", "bgm")
							else
								TEsound.play("sound/bgm/"..p.name..".ogg", "bgm")
							end
							TEsound.volume("bgm", 0.5)
							self.currentbgm = p.name
						end,
	}
	switch[m]()
end

function pager:cancelPlayback()
	about.session.cslot.bgm.name = "none"
	TEsound.stop("bgm")
end

function pager:wait(p)
	local t = p.time or 3
	self.timer:add(t, function()	self.state = 0	end)
end

function pager:fill(p)
	local m = p.mode or "in"
	local r = p.r or 0
	local g = p.g or 0
	local b = p.b or 0
	self.color.r, self.color.g, self.color.b = r, g, b
	if not p.instant then
		if m == "in" then
			local t = p.time or 3
			self.timer:tween(t, self.color, { alpha = 255 }, "linear", function() self.color.alpha = 255	self.state = 0	end)		
		elseif m == "out" then
			local t = p.time or 3
			self.timer:tween(t, self.color, { alpha = 0 }, "linear", function()	self.color.alpha = 0	self.state = 0	end)
		end
	else
		if m == "in" then			
			self.color.alpha = 255
		elseif m == "out" then
			self.color.alpha = 0
		end
		self.state = 0
	end
end

function pager:destroy()
	self.input:destroy()
end

function pager:rewind(mode, sc, l)		--// complete or partial (0 or 1)
	local m = mode or 1
	local s = about.session.cslot.shadow
	local switch = {
		[0] = function()
			local s, rewind_script = about.session.cslot.shadow, {}
			for k,v in pairs(s) do
				table.insert(rewind_script, v)
			end
			if rewind_script[#rewind_script][1] ~= "input" then
				for k2, v2 in pairs(rewind_script) do
					if k2 == "input" then
						k2, v2 = nil, nil
					end
				end
			end
			for k3, v3 in pairs(rewind_script) do
				if type(v3[2]) == "table" then
					v3[2].instant = true
				end
print(v3[1], v3[2], v3[3])				self.cmd[v3[1]](v3[2], v3[3])
			end
			self:open(sc, l)
			self:read()
		end,
		[1] = function()
			local s = self.script[self.current_line - 1]
			if not s then s = self.script[self.current_line] end
			if s and s[1] == "input" then				-- and s[2]["type"] == 1 then
				self.current_line = self.current_line - 1
				self:read()
			end	
		end,
	}
	switch[m]()
end

function pager:unlockAchievement(params)		--// type (img or bgm) and id
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = "<empty>" },
			[2] = { scene = "<empty>" },
			[3] = { scene = "<empty>" },
			[4] = { scene = "<empty>" },
			[5] = { scene = "<empty>" },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, },
			ed	= { [1] = 0, [2] = 0 },
		},
	}				
	if love.filesystem.exists("brm-save") then
		local data = love.filesystem.read("brm-save")
		savefile = json.decode(data)
	end
	local notify = function()
		self.notify = true
		TEsound.play(self.notify_sound, "notification", 0.3)
		self.timer:tween(0.5, self, { notify_alpha = 255 }, "linear", function()
			self.timer:tween(3, self, { notify_alpha = 0 }, "linear", function()
				self.notify = false
				self.notify_text = ""
			end)
		end)
	end
	local switch = {
		[1] = function()
			if savefile.achievements.img[params.id] then
				if savefile.achievements.img[params.id] == 0 then
					savefile.achievements.img[params.id] = 1	
					self.notify_text = "New Image has been unlocked!"
					love.filesystem.write("brm-save", json.encode(savefile))
					notify()
				end
			else
				savefile.achievements.img[params.id] = 1	
				self.notify_text = "New Image has been unlocked!"
				love.filesystem.write("brm-save", json.encode(savefile))
				notify()
			end
		end,
		[2] = function()
			if savefile.achievements.bgm[params.id] then
				if savefile.achievements.bgm[params.id] == 0 then
					savefile.achievements.bgm[params.id] = 1	
					self.notify_text = "New BGM has been unlocked!"
					love.filesystem.write("brm-save", json.encode(savefile))
					notify()
				end
			else
				savefile.achievements.bgm[params.id] = 1	
				self.notify_text = "New BGM has been unlocked!"
				love.filesystem.write("brm-save", json.encode(savefile))
				notify()
			end
		end,
		[3] = function()
			if savefile.achievements.ed and savefile.achievements.ed[params.id] then
				if savefile.achievements.ed[params.id] == 0 then
					savefile.achievements.ed[params.id] = 1
					love.filesystem.write("brm-save", json.encode(savefile))
				end
			else
				if not savefile.achievements.ed then savefile.achievements.ed = { [1] = 0, [2] = 0 }	end
				savefile.achievements.ed[params.id] = 1
				love.filesystem.write("brm-save", json.encode(savefile))
			end
		end
	}
	switch[params.type]()
end

function pager:correct(opts)	--// opts = { [1] - fill, [2] - text }
	if opts[1] == true then		--// clear the screen
		self.color.alpha = 0
	end
	if opts[2] == true then
		self.typewriter.textcolor = {	r = 255, g = 255, b = 255, alpha = 255	}
	end
end

function pager:addLayer(ld)	--// { name, image, (other params) }
	self.layers[#self.layers + 1] = layer(ld)
end

function pager:removeLayer(ln)	--// name
	local id
	for i = 1, #self.layers, 1 do
		if self.layers[i].name == ln then
			id = i
		end
	end
	if id then
		tremove(self.layers, id)
	end
end

function pager:setLayer(ln, lp) --// name, various params
	local id
	for i = 1, #self.layers, 1 do
		if self.layers[i].name == ln then
			id = i
		end
	end
	if id then
		self.layers[id]:set(lp)
	end
end

function pager:fadeLayer(ln, lp)
	local id
	for i = 1, #self.layers, 1 do
		if self.layers[i].name == ln then
			id = i
		end
	end
	if id then
		local d = lp.dir or "in"
		local t = lp.time or 2
		local switch = {
			["in"] 		= function()	self.layers[id]:hide(t)	end,
			["out"] 	= function()	self.layers[id]:show(t)	end,
		}
		switch[d]()
	end
end