local br = {}	-- a table of objects I'd rather load once than every time the gamestate changes
local mpow, mfloor, sbyte = math.pow, math.floor, string.byte

br.gpColor = { fg = { 156, 156, 156 }, bg = { 181, 181, 181, 75 } }
br.uiColor = { fg = {255, 255, 255 }, bg = {255, 255, 255, 50 } }
br.activeColor = { 0, 191, 255, 75 }

br.background = love.graphics.newImage("graphics/static/background.jpg")

br.sfx = {
	[1] = love.sound.newSoundData("sound/sfx/click.ogg"),
	[2] = love.sound.newSoundData("sound/sfx/switch.ogg"),
	[3] = love.sound.newSoundData("sound/sfx/se107.ogg"),
	[4] = love.sound.newSoundData("sound/sfx/se141.ogg"),
	[5] = love.sound.newSoundData("sound/sfx/se082.ogg"),
	[6] = love.sound.newSoundData("sound/sfx/se077.ogg"),
	[7] = love.sound.newSoundData("sound/sfx/se215.ogg"),
	[8] = love.sound.newSoundData("sound/sfx/se032.ogg"),
	[9] = love.sound.newSoundData("sound/sfx/se122.ogg"),
	[10] = love.sound.newSoundData("sound/sfx/se414.ogg"),
	[11] = love.sound.newSoundData("sound/sfx/notification.ogg"),
}

br.math_round = function( roundIn , roundDig )
	
	if not roundDig then roundDig = 2 end
	
	local mul = mpow( 10, roundDig )
    return ( mfloor( ( roundIn * mul ) + 0.5 ) / mul )

end

br.sW, br.sH = love.graphics.getWidth(), love.graphics.getHeight()
br.sc = br.math_round(br.sH / 864, 3)

if br.sc ~= 1 then
	br.cx, br.cy = br.sW / 2 - 576 * br.sc, 0
	br.xc, br.yc = br.sW / 2 + 576 * br.sc, br.sH
else
	br.cx, br.cy, br.xc, br.yc = 0, 0, 1152, 864
end

br.za = {			--// new coords were manifested because page origin changed from top-left to center
	["top-left"] = { x = 384 * br.sc, y = 288 * br.sc },
	["top-center"] = { x = 0, y = 288 * br.sc },
	["top-right"] = { x = -384 * br.sc, y = 288 * br.sc },
	["center-left"] = { x = 384 * br.sc, y = 0 },
	["center-center"] = { x = 0, y = 0 },
	["center-right"] = { x = -384 * br.sc , y = 0 },
	["bottom-left"] = { x = 384 * br.sc, y = -288 * br.sc },
	["bottom-center"] = { x = 0, y = -288 * br.sc },
	["bottom-right"] = { x = -384 * br.sc, y = -288 * br.sc },
}

br.fonts = {
	["regular"] = love.graphics.newFont("graphics/fonts/FiraSans-Regular.otf", 32 * br.sc),
	["large"] = love.graphics.newFont("graphics/fonts/FiraSans-Regular.otf", 64 * br.sc),
}

br.drawFPS = function()
	love.graphics.print("FPS: "..love.timer.getFPS(), br.cx + 8 * br.sc, _, _, 0.5 * br.sc, 0.5 * br.sc)
	love.graphics.print("brmonogatari v"..about.ver, br.xc - 155 * br.sc, br.yc - 32 * br.sc, _, 0.5 * br.sc, 0.5 * br.sc)
end

return br