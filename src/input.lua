--// Player input controller bound to pager class.
--// Represents an abstraction layer between player input and various available actions.

local class = require "libs/hump/class"
local timer = require "libs/hump/timer"
local mceil = math.ceil

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc

input = class {}

function input:init()
	
	self.font_large = game.fonts["large"]
	self.state = 0	--// 0 - free, 1 - busy
	self.dbstyle = 1
	self.color_single = { 0, 0, 0, 0 }
	self.type = 0	--// 0 - single, 1 - multiple
	self.buttons = {}
	self.pos = {
		x = cx + 105 * sc,
		w = 740 * sc,
	}
	
	self.color_multiple = {
		[1] = {
			bg = { 255, 255, 255, 25 },
			fg = { 255, 255, 255, 255 },
			active = { 0, 191, 255, 35 },
			
		},
		[2] = {
			bg = { 0, 0, 0, 64 },
			fg = { 0, 0, 0, 64 },
			active = { 22, 194, 251, 96 },
		},
	}
end

function input:update(dt)
	gooi.update(dt)
	if #self.buttons > 1 then	gooi.get(self.buttons[self.active]).bgColor = self.color_multiple[self.dbstyle].active	end
end

function input:draw()
	gooi.draw("input")
--[[	if self.state == 1 and self.type == 0 then
		love.graphics.setColor(self.color_active[1], self.color_active[2], self.color_active[3], 255)
		love.graphics.print("<...>", sW / 2 - 12 * sc, sH / 2 + 256 * sc, 0, sc, sc)
	end	]]
end

function input:keypressed(key, unicode, handle)
	if handle.state == 1 then
		if handle.type == 0 then
			TEsound.play(game.sfx[1], "click", 0.6) 
			table.remove(handle.buttons, 1) 
			gooi.remove("single") 
			handle.state = 0
		elseif handle.type == 1 then
			local switch = {
				["up"] = function()	TEsound.play(game.sfx[2], "switch", 0.6)	handle.onKeyUp()	end,
				["down"] = function()	TEsound.play(game.sfx[2], "switch", 0.6)	handle.onKeyDown()	end,
				["w"] = function()	TEsound.play(game.sfx[2], "switch", 0.6)	handle.onKeyUp()	end,
				["s"] = function()	TEsound.play(game.sfx[2], "switch", 0.6)	handle.onKeyDown()	end,
				["return"] = function()	handle.action(handle.active)	end,
				[" "] = function()	handle.action(handle.active)	end,
				["e"] = function()	handle.action(handle.active)	end,
				["x"] = function()	handle.action(handle.active)	end,
				["1"]  = function()	handle.action(1)				end,
				["2"]  = function()	handle.action(2)				end,
				["3"]  = function()	handle.action(3)	end,
				["4"]  = function()	handle.action(4)	end,
				["kp1"]  = function()	handle.action(1)			end,
				["kp2"]  = function()	handle.action(2)	end,
				["kp3"]  = function()	handle.action(3)	end,
				["kp4"]  = function()	handle.action(4)	end,
			}
			if switch[key] then switch[key]()	end			
		end
	end
end

function input:joystickpressed(joystick, button, handle)
	if handle.state == 1 then
		if handle.type == 0 then
			TEsound.play(game.sfx[1], "click", 0.6) 
			table.remove(handle.buttons, 1) 
			gooi.remove("single") 
			handle.state = 0
		elseif handle.type == 1 then
			local switch = {
				[1] = function()	handle.action(handle.active)	end,
				[2] = function()	handle.action(handle.active)	end,
				[3] = function()	handle.action(handle.active)	end,
				[4] = function()	handle.action(handle.active)	end,
				[5] = function()	handle.onKeyUp()	end,
				[6] = function()	handle.onKeyDown()	end,
				[7] = function()	handle.onKeyUp()	end,
				[8] = function()	handle.onKeyDown()	end,
			}
			if switch[button] then switch[button]()	end	
		end
	end
end

function input:single()
	self.type = 0
	gooi.newButton("single", _, 0, 0, sW, sH, _, "input")				--// button origins: ( cs + 176 * sc, 132 * sc, 800 * sc, 600 * sc )
	gooi.get("single").fgColor, gooi.get("single").bgColor = self.color_single, self.color_single
	gooi.get("single").showBorder = false
	table.insert(self.buttons, "single")
	gooi.get("single"):onRelease(function() TEsound.play(game.sfx[1], "click", 0.6) table.remove(self.buttons, 1) gooi.remove("single") self.state = 0	end)
end

function input:multiple(m, lines)	
	self.type = 1
	self.action =  function(id)
		if id <= #self.buttons then
			TEsound.play(game.sfx[1], "click", 0.6)
			for i = 1, #self.buttons, 1 do
				table.remove(self.buttons, i)
				gooi.remove("multiple-"..i)
			end
			gooi.remove("multiple-grid")
			self.active, self.actions = nil, nil 
			self.onKeyUp, self.onKeyDown = nil, nil
			about.session.cslot.script = m[id]["name"]
			about.session.cslot.line = 1
			GS.switch(require("states/gamescreen"))
		end
	end
	
	self.onKeyUp = function()
		TEsound.play(game.sfx[2], "switch", 0.6)
		gooi.get(self.buttons[self.active]).bgColor = self.color_multiple[self.dbstyle].bg		
		if self.active - 1 >= 1 then
			self.active = self.active - 1	
		else
			self.active = #self.buttons		
		end	
	end
	
	self.onKeyDown = function()
		TEsound.play(game.sfx[2], "switch", 0.6)
		gooi.get(self.buttons[self.active]).bgColor = self.color_multiple[self.dbstyle].bg	
		if self.active + 1 <= #self.buttons then			
			self.active = self.active + 1
		else
			self.active = 1			
		end		
	end
	
	local correction = { ["en"] = 50, ["ru"] = 70 }
	local height, add_delta = {}, {}
	for i = 1, #m, 1 do
		local l = mceil((#m[i]["text"] + 3)/correction[about.settings.lang])
		if l > 1 then
			table.insert(height, (64 * l) * sc)
			add_delta[i + 1] =  (32 * l) * sc
		else
			table.insert(height, 64 * sc)
		end
	end
		
	if self.dbstyle == 1 then
		lines = lines + 2
		local cap = 800/12 * lines
		gooi.newPanel("multiple-grid", self.pos.x, cap, self.pos.w, 645 * sc - cap, "grid "..#m.."x1")
		for i = 1, #m, 1 do
			gooi.get("multiple-grid"):add(
				gooi.newButton("multiple-"..i, i..". "..m[i]["text"], _, _, _, _, _, "input")
			)
			gooi.get("multiple-"..i).fgColor, gooi.get("multiple-"..i).bgColor = self.color_multiple[self.dbstyle].fg, self.color_multiple[self.dbstyle].bg
			gooi.get("multiple-"..i).showBorder, gooi.get("multiple-"..i).uses_printf = false, true
			gooi.get("multiple-"..i):onRelease(function() self.action(i) end)
			table.insert(self.buttons, "multiple-"..i)
		end
	else
		gooi.newPanel("multiple-grid", self.pos.x, 15 * sc, self.pos.w, 454 * sc, "grid "..#m.."x1")
		for i = 1, #m, 1 do
			gooi.get("multiple-grid"):add(
				gooi.newButton("multiple-"..i, i..". "..m[i]["text"], _, _, _, _, _, "input")
			)
			gooi.get("multiple-"..i).fgColor, gooi.get("multiple-"..i).bgColor = self.color_multiple[self.dbstyle].fg, self.color_multiple[self.dbstyle].bg
			gooi.get("multiple-"..i).showBorder, gooi.get("multiple-"..i).uses_printf, gooi.get("multiple-"..i).align = false, true, "center"
			gooi.get("multiple-"..i):onRelease(function() self.action(i) end)
			table.insert(self.buttons, "multiple-"..i)
		end
	end
	self.active = 1
end

function input:destroy()
	gooi.remove("multiple-grid")
	for i = 1, #self.buttons, 1 do
		gooi.remove(self.buttons[i])
	end
	self.buttons = {}	
end