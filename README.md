# About
Blue Ribbons Monogatari is a joke VN that adapts first three Blue Ribbons threads in two routes.

# Current version:
1.2

# Source:
* http://foolz.fireden.net/a/thread/115493076
* http://foolz.fireden.net/a/thread/115534184
* http://foolz.fireden.net/a/thread/115560076

# Download links:
* Windows x32/x64 (installer) - https://bitbucket.org/blueribbonsproject/brm-local/downloads/BlueRibbonsMonogatari_1.2_setup.exe
* Windows x32/x64 (zip) - https://bitbucket.org/blueribbonsproject/brm-local/downloads/Blue%20Ribbon%20Monogatari%20v1.2.zip
* Android (+Ouya) - https://bitbucket.org/blueribbonsproject/brm-local/downloads/brm-1-2-release.apk
* Web Player - https://blueribbons.aerobatic.io/play/brm.html

# Known issues:
* too short - I know.

# Changelog:
## 1.2
* improved design;
* resolved UI issues;
* additional content and achievements.
## 1.1
* brand new Windows installer (HOT!);
* various bug fixes and cosmetic changes;
* package size reduced by 25%.
## 1.0
* fixed endless loop in route 1;
* fixed load menu when switching between languages.
## 1.0-pv
* initial release.