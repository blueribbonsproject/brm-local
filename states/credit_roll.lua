local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local credit_roll = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc

function credit_roll:init()
	self.print = {}
	self.font = game.fonts["regular"]
	self.logo = love.graphics.newImage("graphics/static/logo_liminalia2.png")
end

function credit_roll:enter(state, mode)
	about.scene = "credit_roll"
	
	self.mode = mode
	self.alpha = 255
	self.ty1 = sH
	self.ty2 = sH
	self.ly = sH + 130 * sc
	self.state = 0
	self.text_buffer_1 = ""
	self.text_buffer_2 = ""
	self.timer = timer.new()
	
	if self.mode == 1 then
		t = 25
		local upperlim = -(sH * 2)
		local data
		if love.filesystem.exists("brm-save") then
			local raw = love.filesystem.read("brm-save")
			data = json.decode(raw)
			data.route2 = 1		--// enable route "Rebuild"
		else
			data = {
				route2 = 1, 
				slots = { 
					[1] = { scene = "<empty>" },
					[2] = { scene = "<empty>" },
					[3] = { scene = "<empty>" },
					[4] = { scene = "<empty>" },
					[5] = { scene = "<empty>" },
				},
				achievements = { 
					img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, }, 
					bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, },
					ed	= { [1] = 0, [2] = 0 },
				},		
			}
		end
		love.filesystem.write("brm-save", json.encode(data))

		self.text_buffer_1 = "ORIGINAL CREATOR\n\
		/a/nonymous (SAE Works)\n\
		\n\
		\n\
		SOURCE\n\
		https://archive.moe/a/thread/115493076/\n\
		\n\
		\n\
		CG DESIGN AND GRAPHICS\n\
		/a/nonymous (SAE Works)\n\
		\n\
		\n\
		\n\
		\n\
		Route 'Rebuild' has been unlocked!\
		"
		self.text_buffer_2 = "See you in the Act II!"
		
		self.bgm = TEsound.play("sound/bgm/why_or_why_not.ogg", "bgm")
		TEsound.volume("bgm", 0.5)
		
		self.timer:tween(15, self, { ty1 = upperlim }, "linear", function()
			self.timer:tween(5, self, { ty2 = sH / 2 }, "linear", function()
				self.timer:tween(4, self, { alpha = 0 }, "linear")
			end)
		end)
		
		self.timer:addPeriodic(25, function()
			TEsound.stop("bgm")
			TEsound.play("sound/bgm/kimi_no_iru_machi.ogg", "bgm")
			GS.switch(require("states/mainmenu"))
		end)
		
	elseif self.mode == 2 then
		t = 55		
		local upperlim = -sH * 5 - 128 * sc
		
		if love.filesystem.exists("brm-save") then
			local raw = love.filesystem.read("brm-save")
			data = json.decode(raw)
			data.route3 = 1		--// enable route "Rebuild"
		else
			data = {
				route2 = 1,
				route3 = 1,
				slots = { 
					[1] = { scene = "<empty>" },
					[2] = { scene = "<empty>" },
					[3] = { scene = "<empty>" },
					[4] = { scene = "<empty>" },
					[5] = { scene = "<empty>" },
				},
				achievements = { 
					img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, }, 
					bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, },
					ed	= { [1] = 0, [2] = 0 },
				},		
			}
		end
		love.filesystem.write("brm-save", json.encode(data))
		
		self.text_buffer_1 = "ORIGINAL CREATOR\n\
		/a/nonymous (SAE Works)\n\
		\n\
		\n\
		SOURCE\n\
		https://archive.moe/a/thread/115493076/\nhttps://archive.moe/a/thread/115534184/\nhttps://archive.moe/a/thread/115560076/\n\
		\n\
		\n\
		CG DESIGN AND GRAPHICS\n\
		/a/nonymous (SAE Works)\n\
		\n\
		\n\
		BACKGROUND MUSIC\n\
		Fujita Junpei - Kimi no Iru Machi (Grisaia no Kajitsu OST)\
		Tokisawa Nao - Strange Feeling (Gokukoku no Brynhildr OST)\
		Tokisawa Nao - Fatal Scene (Gokukoku no Brynhildr OST)\
		Tokisawa Nao - School Days (Gokukoku no Brynhildr OST)\
		Tokisawa Nao - Nokosare ta Jikan (Gokukoku no Brynhildr OST)\
		Konishi Kayo, Kondoo Yukio - Katsubou (Elfen Lied OST)\
		Haga Keita - Ever-present Feeling (Fate/stay night OST)\
		Haga Keita - Motome Au Kokoro (Fate/stay night OST)\
		Kajiura Yuki - Silver Moon (Fate/Zero OST)\
		Iwasaki Motoyoshi - Angeloid no Senaka (Sora no Otoshimono OST)\
		Hashimoto Yukari - Lost My Pieces (Toradora OST)\
		ORANGE RANGE - Asterisk (Bleach OP1)\
		FLOW - Sign (Naruto Shippuuden OP6)\
		Linked Horizon - Guren no Yumia (Shingeki no Kyojin OP1)\
		Oshima Hiroyuki feat.Katakiri Rekka - Why, Or Why Not (Higurashi no Naku Koro ni ED)\
		ClariS - CLICK (Nisekoi OP) (piano cover by yoshiki - http://www.youtube.com/channel/UC_2qXn25z1EZM_0vB-90R_g)\n\
		\n\
		\n\
		SOUND EFFECTS\n\
		/a/nonymous (Liminalia)\nFate/stay night OST\n\
		\n\
		\n\
		PROGRAMMING\n\
		/a/nonymous (Liminalia)\n\
		\n\
		\n\
		LÖVE\nhttps://love2d.org/\n\
		\n\
		\n\
		LIBRARIES\n\
		David Kolf's JSON module for Lua 5.1/5.2\
		GOOi by Tavo\
		HUMP by vrld\
		TEsound by Ensayia and Taehl\
		\n\
		\n\
		\n\
		\n\
		\n\
		"
		self.text_buffer_2 = "The End\n[spoiler]:^)[/spoiler]"		
		self.bgm = TEsound.play("sound/bgm/why_or_why_not2.ogg", "bgm")
		TEsound.volume("bgm", 0.5)
		self.timer:tween(42, self, { ty1 = upperlim }, "linear", function()
		end)
		
		self.timer:addPeriodic(35.5, function()
			self.timer:tween(45, self, { ly = upperlim }, "linear")
		end)
		
		self.timer:addPeriodic(42,function()
			self.timer:tween(7, self, { ty2 = sH / 2 }, "linear", function()
				self.timer:tween(4, self, { alpha = 0 }, "linear")
			end)
		end)
		
		self.timer:addPeriodic(54, function()
			TEsound.stop("bgm")
			TEsound.play("sound/bgm/kimi_no_iru_machi.ogg", "bgm")
			GS.switch(require("states/mainmenu"))
		end)
		
	end
	
end

function credit_roll:leave()

	self.timer:clear()
end

function credit_roll:update(dt)

	self.timer:update(dt)
end

function credit_roll:draw()
	love.graphics.draw(self.logo, sW / 2, self.ly, 0, sc * 0.8, sc * 0.8, self.logo:getWidth() / 2, self.logo:getHeight() / 2)
	love.graphics.setColor(255, 255, 255, self.alpha)
	love.graphics.setFont(game.fonts["regular"])
	love.graphics.printf(self.text_buffer_1, 0, self.ty1, sW, 'center')
	love.graphics.printf(self.text_buffer_2, 0, self.ty2, sW, 'center')
end

return credit_roll