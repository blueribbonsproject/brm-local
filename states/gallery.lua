local json = require("libs/dkjson")
local timer = require("libs/hump/timer")
local gallery = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function gallery:init()
	self.activeColor = game.activeColor
	self.sfx = game.sfx
	self.timer = timer.new()	
end

function gallery:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "gallery"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = "<empty>" },
			[2] = { scene = "<empty>" },
			[3] = { scene = "<empty>" },
			[4] = { scene = "<empty>" },
			[5] = { scene = "<empty>" },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, },
			ed	= { [1] = 0, [2] = 0 },
		},
	}				
	if love.filesystem.exists("brm-save") then
		local data = love.filesystem.read("brm-save")
		savefile = json.decode(data)
	end
	
	local labels = {
		[1] = {
			[1] = "Doushio",		--// 1/25
			[2] = "graphics/static/i25.png",
		},
		[2] = {
			[1] = "Pantsu Shot",		--// 1/28
			[2] = "graphics/static/i28.png",
		},
		[3] = {
			[1] = "Goodnight",		--// 2/4-1
			[2] = "graphics/static/i4-1.png",
		},
		[4] = {
			[1] = "Shinu",		--// 2/4-2
			[2] = "graphics/static/i4-2.png",
		},
		[5] = {
			[1] = "Ahegao",		--//	2/9
			[2] = "graphics/static/i9.png",
		},
		[6] = {
			[1] = "Green Route",		--// 2/18-2
			[2] = "graphics/static/i18-2.png",
		},
		[7] = {
			[1] = "BTFO",		--// 2/18-3
			[2] = "graphics/static/i-btfo.png",
		},
		[8] = {
			[1] = "Bread Exploded",		--// 2/26
			[2] = "graphics/static/i26.png",
		},
		[9] = {
			[1] = "Nisemono Lovers",		--// 1/30
			[2] = "graphics/static/i30.png",
		},
		[10] = {
			[1] = "Ahegao 2",		--// 2 /37
			[2] = "graphics/static/i37.png",
		},
		[11] = {
			[1] = "Majou Shoujo",		--// 4 /34
			[2] = "graphics/static/i-4-34.png",
		},
		[12] = {
			[1] = "Pantsu Shot 2",		--// 4 /39
			[2] = "graphics/static/i-4-39.png",
		},
	}
	
	self.menu = { buttons = {}, active = 1 }
	
	self.src = {
		[1] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-1/25",
			[4] = "graphics/static/locked.png",
		},
		[2] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-1/28",
			[4] = "graphics/static/locked.png",
		},
		[3] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/4-1",
			[4] = "graphics/static/locked.png",
		},
		[4] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/4-2",
			[4] = "graphics/static/locked.png",
		},
		[5] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/9",
			[4] = "graphics/static/locked.png",
		},
		[6] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/18-2",
			[4] = "graphics/static/locked.png",
		},
		[7] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "static/btfo",
			[4] = "graphics/static/locked.png",
		},
		[8] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/26",
			[4] = "graphics/static/locked.png",
		},
		[9] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-1/30",
			[4] = "graphics/static/locked.png",
		},
		[10] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-2/37",
			[4] = "graphics/static/locked.png",
		},
		[11] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-4/34",
			[4] = "graphics/static/locked.png",
		},
		[12] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "cg-4/39",
			[4] = "graphics/static/locked.png",
		},
	}
	
	for i = 1, 12, 1 do
		if savefile.achievements.img[i] then
			if savefile.achievements.img[i] == 1 then
				self.src[i][1] = 1
				self.src[i][2] = labels[i][1]
				self.src[i][4] = labels[i][2]
			end
		end
	end
	
	self.action = function(id)
		if id ~= 13 then
			if self.src[id][1] == 1 then
				TEsound.play(self.sfx[1], "click", 0.6)
				GS.switch(require("states/imgviewer"), "graphics/"..self.src[id][3]..".jpg")
			else
				TEsound.play(self.sfx[1], "click", 0.6)
			end
		else
			self.quit()
		end
	end
	
	self.quit = function()
		TEsound.play(self.sfx[1], "click", 0.6)	
		GS.switch(require("states/mainmenu"))
	end
	
	gooi.newPanel("gallery-grid", cx + 40 * sc, 40 * sc, (xc - cx) - 80 * sc, sH/4 * 3, "grid 6x2")
	for i = 1, 12, 1 do
		gooi.get("gallery-grid"):add(
			gooi.newButton("gallery-"..i, self.src[i][2], _, _, _, _, self.src[i][4], _, sc)
		)
		table.insert(self.menu.buttons, "gallery-"..i)
	end
	
	for i = 1, #self.menu.buttons, 1 do	
		gooi.get(self.menu.buttons[i]):onRelease(function() self.action(i) end)
		gooi.get(self.menu.buttons[i]).borderWidth = 5 * sc
	end
	
	gooi.newButton("gallery-13", lang.back, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("gallery-13").howRound = 1
	gooi.get("gallery-13"):generateBorder()
	gooi.get("gallery-13"):onRelease(self.quit)
	table.insert(self.menu.buttons, "gallery-13")
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
end

function gallery:leave()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("gallery-"..i)
	end
	gooi.remove("gallery-grid")
end

function gallery:update(dt)	
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end
	
	self.timer:update(dt)	
end

function gallery:draw()
	gooi.draw()
	drawFPS()	
end

function gallery:keypressed(key, unicode)	
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.action(self.menu.active)	end,
		[" "] = function()	self.action(self.menu.active)	end,
		["e"] = function()	self.action(self.menu.active)	end,
		["x"] = function()	self.action(self.menu.active)	end,
		["escape"] = function()	self.quit()	end,
		["menu"] = function()	self.quit()	end,
	}
	if switch[key] then switch[key]()	end
end

function gallery:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	
	local switch = {
		[1] = function()	self.action(self.menu.active)	end,
		[2] = function()	self.action(self.menu.active)	end,
		[3] = function()	self.action(self.menu.active)	end,
		[4] = function()	self.action(self.menu.active)	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.quit()			end,
	}
	if switch[button] then switch[button]()	end	
end

return gallery