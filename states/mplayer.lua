local json = require("libs/dkjson")
local timer = require("libs/hump/timer")
local mplayer = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function mplayer:init()
	self.activeColor = game.activeColor
	self.sfx = game.sfx
	self.timer = timer.new()
end

function mplayer:enter(state)
	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	about.scene = "mplayer"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end	
	self.current_bgm = 0
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = "<empty>" },
			[2] = { scene = "<empty>" },
			[3] = { scene = "<empty>" },
			[4] = { scene = "<empty>" },
			[5] = { scene = "<empty>" },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, },
			ed	= { [1] = 0, [2] = 0 },
		},
	}
	if love.filesystem.exists("brm-save") then
		local data = love.filesystem.read("brm-save")
		savefile = json.decode(data)
	end
	
	local labels = {
		[1] = "Strange Feeling",
		[2] = "Fatal Scene", 
		[3] = "Katsubou",	
		[4] = "School Days",
		[5] = "Nokosare ta Jikan", 
		[6] = "Ever-present Feeling",
		[7] = "Angeloid no Senaka",
		[8] = "Silver Moon",	
		[9] = "Motome Au Kokoro",
		[10] = "Click (Piano Cover)",
		[11] = "Void",
		[12] = "GSNK Piano OST (Cover)",
	}
	
	self.menu = { buttons = {}, active = 1 }
	
	self.titles = {
		[0] = "Nobody - None",
		[1] = "Tokisawa Nao - Strange Feeling (Gokukoku no Brynhildr OST)",
		[2] = "Tokisawa Nao - Fatal Scene (Gokukoku no Brynhildr OST)",
		[3] = "Konishi Kayo & Kondoo Yukio - Katsubou (Elfen Lied OST)",
		[4] = "Tokisawa Nao - School Days (Gokukoku no Brynhildr OST)",
		[5] = "Tokisawa Nao - Nokosare ta Jikan (Gokukoku no Brynhildr OST)",	
		[6] = "Haga Keita - Ever-present Feeling (Fate/stay night OST)",
		[7] = "Iwasaki Motoyoshi - Angeloid no Senaka (Sora no Otoshimono OST)",
		[8] = "Kajiura Yuki - Silver Moon (Fate/Zero OST)",
		[9] = "Haga Keita - Motome Au Kokoro (Fate/stay night OST)",
		[10] = "ClariS - CLICK (Nisekoi OP) (Piano cover by yoshiki)",
		[11] = "Konishi Kayo & Kondoo Yukio - Void (Elfen Lied OST)",
		[12] = "Gekkan Shoujo Nozaki-kun - Piano OST (Cover)",
	}
	
	self.src = {
		[1] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/strange_feeling.ogg",
		},
		[2] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/fatal_scene.ogg"
		},
		[3] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/katsubou.ogg"
		},
		[4] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/school_days.ogg"
		},
		[5] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/nokosare_ta_jikan.ogg"
		},
		[6] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/bgm16.ogg"
		},
		[7] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/angeloid_no_senaka.ogg"
		},
		[8] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/silver_moon.ogg"
		},
		[9] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/bgm19.ogg"
		},
		[10] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/click_cover_cut.ogg"
		},
		[11] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/void.ogg"
		},
		[12] = {
			[1] = 0,
			[2] = lang.locked,
			[3] = "sound/bgm/gsnk_piano.ogg"
		},
	}
	
	for i = 1, 12, 1 do
		if savefile.achievements.bgm[i] then
			if savefile.achievements.bgm[i] == 1 then
				self.src[i][1] = 1
				self.src[i][2] = labels[i]
			end
		end
	end
	
	self.action = function(id)
		if id ~= 13 then
			if self.src[id][1] == 1 then
				TEsound.play(self.sfx[1], "click", 0.6)
				TEsound.stop("bgm")
				if self.current_bgm == id then
					self.current_bgm = 0
				else
					self.current_bgm = id
					TEsound.play(self.src[id][3], "bgm", _, _, function() self.current_bgm = 0 end)
					TEsound.volume("bgm", 0.5)
				end
			else
				TEsound.play(self.sfx[1], "click", 0.6)
			end
		else
			self.quit()
		end
	end
	
	self.quit = function()
		TEsound.play(self.sfx[1], "click", 0.6)	
		TEsound.stop("bgm")
		TEsound.playLooping("sound/bgm/kimi_no_iru_machi.ogg", "bgm")
		TEsound.volume("bgm", 0.5)
		GS.switch(require("states/mainmenu"))
	end
	
	gooi.newPanel("mplayer-grid", cx + 40 * sc, 180 * sc, (xc - cx) - 80 * sc, sH/2, "grid 6x2")
	for i = 1, 12, 1 do
		gooi.get("mplayer-grid"):add(
			gooi.newButton("mplayer-"..i, self.src[i][2])
		)
		table.insert(self.menu.buttons, "mplayer-"..i)
	end
	
	for i = 1, #self.menu.buttons, 1 do	
		gooi.get(self.menu.buttons[i]):onRelease(function() self.action(i) end)
		gooi.get(self.menu.buttons[i]).borderWidth = 5 * sc
	end
	
	gooi.newButton("mplayer-13", lang.back, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("mplayer-13").howRound = 1
	gooi.get("mplayer-13"):generateBorder()
	gooi.get("mplayer-13"):onRelease(self.quit)
	table.insert(self.menu.buttons, "mplayer-13")
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor, gooi.get(self.menu.buttons[self.menu.active]).borderColor = game.style.bgColor, game.style.borderColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor, gooi.get(self.menu.buttons[self.menu.active]).borderColor = game.style.bgColor, game.style.borderColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor, gooi.get(self.menu.buttons[self.menu.active]).borderColor = game.style.bgColor, game.style.borderColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor, gooi.get(self.menu.buttons[self.menu.active]).borderColor = game.style.bgColor, game.style.borderColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false	
end

function mplayer:leave()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("mplayer-"..i)
	end
	gooi.remove("mplayer-grid")
end

function mplayer:update(dt)	
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end		
	end
	self.timer:update(dt)	
end

function mplayer:draw()
	love.graphics.printf(self.titles[self.current_bgm], 0, 86 * sc, sW, "center")
	gooi.draw()
	drawFPS()	
end

function mplayer:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.action(self.menu.active)	end,
		[" "] = function()	self.action(self.menu.active)	end,
		["e"] = function()	self.action(self.menu.active)	end,
		["x"] = function()	self.action(self.menu.active)	end,
		["escape"] = function()	self.quit()	end,
		["menu"] = function()	self.quit()	end,
	}
	if switch[key] then switch[key]()	end
end

function mplayer:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end	
	local switch = {
		[1] = function()	self.action(self.menu.active)	end,
		[2] = function()	self.action(self.menu.active)	end,
		[3] = function()	self.action(self.menu.active)	end,
		[4] = function()	self.action(self.menu.active)	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.quit()			end,
	}
	if switch[button] then switch[button]()	end	
end
return mplayer