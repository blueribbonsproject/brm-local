local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local edlist = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function edlist:init()
	self.activeColor = game.activeColor
	self.font = game.fonts["large"]
	self.timer = timer.new()
	self.sfx = game.sfx	
end

function edlist:enter(state)
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	about.scene = "edlist"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = "<empty>" },
			[2] = { scene = "<empty>" },
			[3] = { scene = "<empty>" },
			[4] = { scene = "<empty>" },
			[5] = { scene = "<empty>" },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, },
			ed	= { [1] = 0, [2] = 0 },
		},
	}				
	if love.filesystem.exists("brm-save") then
		local data = love.filesystem.read("brm-save")
		savefile = json.decode(data)
	end
	
	local src = {
		[1] = { [1] = "graphics/static/ed1.png", [2] = "BRM True End" },
		[2] = { [1] = "graphics/static/ed2.png", [2] = "BRM Rebuild End" },
	}
	
	local images = {
		[1] = "graphics/static/locked.png",
		[2] = "graphics/static/locked.png",
	}
	
	local labels = {
		[1] = lang.locked,
		[2] = lang.locked,
	}
	
	if savefile.achievements.ed then
		for i = 1, #savefile.achievements.ed, 1 do 
			if savefile.achievements.ed[i] == 1 then
				images[i] = src[i][1]
				labels[i] = src[i][2]
			end
		end
	else
		savefile.achievements.ed = { [1] = 0, [2] = 0 }
	end
	
	self.menu = { buttons = {}, active = 1 }	

	self.menu.actions = {
		[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	end,
		[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	end,
		[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/mainmenu"))	end,
	}
	
	gooi.newPanel("edlist-grid", cx + 170 * sc, 100 * sc, (xc - cx) - 340 * sc, sH/3 * 2, "grid 2x2")
	for i = 1, #images, 1 do
		gooi.get("edlist-grid"):add(
			gooi.newButton("edlist-"..i, _, _, _, _, _, images[i], _, sc)
		)
		gooi.get("edlist-"..i).showBorder = false
		gooi.get("edlist-"..i).howRound = 0
		gooi.get("edlist-"..i):generateBorder()
		table.insert(self.menu.buttons, "edlist-"..i)
	end	
--[[	for i = 1, #labels, 1 do
		gooi.get("edlist-grid"):add(
			gooi.newLabel("edlist-"..(i + #images), labels[i], _, _, _, _, _, "left")
		)
		gooi.get("edlist-"..i).showBorder = false
		gooi.get("edlist-"..i).howRound = 0
		gooi.get("edlist-"..i):generateBorder()
		table.insert(self.menu, "edlist-"..(i + #images))
	end	]]
	
	gooi.newButton("edlist-"..(#self.menu.buttons + 1), lang.back, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("edlist-"..(#self.menu.buttons + 1)):onRelease(function() self.menu.actions[3]() end)
	gooi.get("edlist-"..(#self.menu.buttons + 1)).howRound = 1
	gooi.get("edlist-"..(#self.menu.buttons + 1)):generateBorder()
	table.insert(self.menu.buttons, "edlist-"..(#self.menu.buttons + 1))
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
end

function edlist:leave()	
	self.timer:clear()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("edlist-"..i)
	end
	gooi.remove("edlist-grid")
end

function edlist:update(dt)
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	local function joystickHandler()	
		self.joystickBlocked = true
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end	
	if self.joystickBlocked == false then
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end	
	end
	self.timer:update(dt)	
end

function edlist:draw()
	gooi.draw()
	drawFPS()
end

function edlist:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	self.menu.actions[3]()	end,
		["menu"] = function()	self.menu.actions[3]()	end,
	}
	if switch[key] then switch[key]()	end
end

function edlist:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end	
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.menu.actions[3]()	end
	}
	if switch[button] then switch[button]()	end
end
return edlist