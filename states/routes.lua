local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local routes = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function routes:init()
	self.activeColor = game.activeColor
	self.font = game.fonts["large"]
	self.timer = timer.new()
	self.sfx = game.sfx	
end

function routes:enter(state)
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	about.scene = "routes"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	self.menu = {	buttons = {}, active = 1	}
	
	local isRouteUnlocked = {
		false, false
	}
	if love.filesystem.exists("brm-save") then
		local raw = love.filesystem.read("brm-save")
		local data = json.decode(raw)
		if data.route2 == 1 then
			isRouteUnlocked[1] = true
		end
		if data.route3 == 1 then
			isRouteUnlocked[2] = true
		end
	end

	self.menu.actions = {
		[1]	=	function()	TEsound.play(self.sfx[1], "click", 0.6)	
							TEsound.stop("bgm")	
							about.session.cslot = {}
							about.session.cslot.route = 1
							about.session.cslot.script = "true-1"
							about.session.cslot.shadow = {}
							GS.switch(require("states/gamescreen"))	
						end,
		[2]	=	function()	
				
				TEsound.play(self.sfx[1], "click", 0.6)		
				if isRouteUnlocked[1] then
					TEsound.stop("bgm")	
					about.session.cslot = {}
					about.session.cslot.route = 2
					about.session.cslot.script = "rebuild-1-2"
					about.session.cslot.shadow = {}
					GS.switch(require("states/gamescreen"))
				end
				
			end,
		[3] =	function()	TEsound.play(self.sfx[1], "click", 0.6)
							if isRouteUnlocked[1] then
								TEsound.stop("bgm")	
								about.session.cslot = {}
								about.session.cslot.route = 3
								about.session.cslot.script = "pandoora-1"
								about.session.cslot.shadow = {}
								GS.switch(require("states/gamescreen"))
							end
						end,
		[4] =	function()	TEsound.play(self.sfx[1], "click", 0.6)	
							if isRouteUnlocked[1] then
								TEsound.stop("bgm")	
								about.session.cslot = {}
								about.session.cslot.route = 4
								about.session.cslot.script = "hs"
								about.session.cslot.shadow = {}
								GS.switch(require("states/gamescreen"))
							end
						end,
		[5]	=	function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/mainmenu"))	end,
	}
	
	gooi.newPanel("menu-grid", cx + 170 * sc, 100 * sc, (xc - cx) - 340 * sc, sH/3 * 2, "grid 2x2"):add(
		gooi.newButton("menu-1", _, _, _, _, _, "graphics/static/route_1.png", _, sc)
	)
	
	if isRouteUnlocked[1] then
		gooi.get("menu-grid"):add(
			gooi.newButton("menu-2", _, _, _, _, _, "graphics/static/route_2.png", _, sc)
		)
	else
		gooi.get("menu-grid"):add(
		gooi.newButton("menu-2", _, _, _, _, _, "graphics/static/locked.png", _, sc)
		)
	end
	if isRouteUnlocked[2] then
		gooi.get("menu-grid"):add(
		gooi.newButton("menu-3", _, _, _, _, _, "graphics/static/pandoora.png", _, sc),
		gooi.newButton("menu-4", _, _, _, _, _, "graphics/static/hs.png", _, sc)
	)
	else
		gooi.get("menu-grid"):add(
		gooi.newButton("menu-3", _, _, _, _, _, "graphics/static/locked.png", _, sc),
		gooi.newButton("menu-4", _, _, _, _, _, "graphics/static/locked.png", _, sc)
	)
	end
	
	
	gooi.newButton("menu-5", lang.back, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("menu-5").howRound = 1
	gooi.get("menu-5"):generateBorder()
	
	table.insert(self.menu.buttons, "menu-1")
	table.insert(self.menu.buttons, "menu-2")
	table.insert(self.menu.buttons, "menu-3")
	table.insert(self.menu.buttons, "menu-4")
	table.insert(self.menu.buttons, "menu-5")
	
	for i = 1, #self.menu.buttons, 1 do		
		gooi.get(self.menu.buttons[i]):onRelease(self.menu.actions[i])
		if i < #self.menu.buttons then
			gooi.get(self.menu.buttons[i]).borderWidth = 5 * sc
			gooi.get(self.menu.buttons[i]).showBorder = false
			gooi.get(self.menu.buttons[i]).howRound = 0
			gooi.get(self.menu.buttons[i]):generateBorder()
		end
	end
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
end

function routes:leave()
	self.timer:clear()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("menu-"..i)
	end
	gooi.remove("menu-grid")
end

function routes:update(dt)	
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end	
	end
	
	self.timer:update(dt)	
end

function routes:draw()
	gooi.draw()
	drawFPS()
end

function routes:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	self.menu.actions[5]()	end,
		["menu"] = function()	self.menu.actions[5]()	end,
	}
	if switch[key] then switch[key]()	end
end

function routes:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end	
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.menu.actions[5]()	end
	}
	if switch[button] then switch[button]()	end
end
return routes