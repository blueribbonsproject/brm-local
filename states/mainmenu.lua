local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local mainmenu = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function mainmenu:init()	
	self.activeColor = game.activeColor
	self.font = game.fonts["large"]
	self.timer = timer.new()
	self.sfx = game.sfx
	self.bgm = TEsound.playLooping("sound/bgm/kimi_no_iru_machi.ogg", "bgm")
	TEsound.volume(self.bgm, 0.5)
	self.esc_color = { 0, 191, 255, 0 }
	self.esc_enabled = false	
end

function mainmenu:enter(state)
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	about.scene = "mainmenu"
	self.esc_text = lang.double_esc	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	self.double_esc = function()
		TEsound.play(self.sfx[1], "click", 0.6)
		if self.esc_enabled then
			if web == true then
				domapi.window.location='../main.html'
			else
				GS.switch(require("states/exit"))	
			end
		else
			self.esc_enabled = true
			self.esc_color[4] = 255
			self.timer:tween(2,  self.esc_color, {	[4] = 0	}, "linear", function() self.esc_enabled = false	end)
		end
	end
	
	local actions = {
		[0] = {
			["true"] = {	-- there is a saved progress
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/menuload"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[4] = function()	
					if web == true then
						domapi.window.location='../main.html'
					else
						GS.switch(require("states/exit"))	
					end
				end,
			},
			["false"] = {
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[3] = function()	
					if web == true then
						domapi.window.location='../main.html'
					else
						GS.switch(require("states/exit"))	
					end
				end,
			}
		},
		[1] = {
			["true"] = {
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/menuload"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/extra"))	end,
				[4] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[5] = function()	
					if web == true then
						domapi.window.location='../main.html'
					else
						GS.switch(require("states/exit"))	
					end
				end,
			},
			["false"] = {
				[1] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/routes"))	end,
				[2]	= function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/extra"))	end,
				[3] = function()	TEsound.play(self.sfx[1], "click", 0.6)	GS.switch(require("states/options"))	end,
				[4] = function()	
					if web == true then
						domapi.window.location='../main.html'
					else
						GS.switch(require("states/exit"))	
					end
				end,
			}
		}
	}
	
	self.menu = { labels = {}, buttons = {}, active = 1 }
	local flag = false
	if love.filesystem.exists("brm-save") == true then
		local data = love.filesystem.read("brm-save")
		local savefile = json.decode(data)
		for i = 1, #savefile.slots, 1 do
			if savefile.slots[i].scene ~= "<empty>" then
				flag = true
			end
		end
	end
	
	self.menu.labels = lang.menu[about.type][tostring(flag)]
	self.menu.actions = actions[about.type][tostring(flag)]
	
	local delta, top = 70, 340
	if #self.menu.labels > 4 then
		top = 270
	end
	
	for i = 1, #self.menu.labels, 1 do
		
		gooi.newButton("menu-"..i, self.menu.labels[i], sW / 2 - 120 * sc, (top + delta * i) * sc, 240 * sc, 60 * sc)
		table.insert(self.menu.buttons, "menu-"..i)
		
	end
	
	for i = 1, #self.menu.buttons, 1 do
		gooi.get(self.menu.buttons[i]):onRelease(self.menu.actions[i])
	end
	
	
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor, gooi.get(self.menu.buttons[self.menu.active]).borderColor = game.style.bgColor, game.style.borderColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor, gooi.get(self.menu.buttons[self.menu.active]).borderColor = game.style.bgColor, game.style.borderColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
end

function mainmenu:leave()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("menu-"..i)
	end	
end

function mainmenu:update(dt)	
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then	
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end
		
	end	
	self.timer:update(dt)	
end

function mainmenu:draw()
	love.graphics.setFont(self.font)
	love.graphics.printf(game.title, 0, 120 * sc, sW, 'center')
	love.graphics.setFont(game.fonts["small"])
	love.graphics.printf("1st Anniversary Edition", 0, 220 * sc, sW, 'center')
	gooi.draw()
	if about.settings.gpswitch then
		gooi.draw("gamepad")
	end
	if self.esc_enabled then
		love.graphics.setColor(self.esc_color)
		love.graphics.printf(self.esc_text, sW / 2 - 200 * sc, 40 * sc, 400 * sc, 'center')
	end
	love.graphics.setColor(255, 255, 255)
	drawFPS()	
end

function mainmenu:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.menu.actions[self.menu.active]()	end,
		[" "] = function()	self.menu.actions[self.menu.active]()	end,
		["e"] = function()	self.menu.actions[self.menu.active]()	end,
		["x"] = function()	self.menu.actions[self.menu.active]()	end,
		["escape"] = function()	self.double_esc()	end,
		["menu"] = function()	self.double_esc()	end,
	}
	if switch[key] then switch[key]()	end
end

function mainmenu:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end	
	local switch = {
		[1] = function()	self.menu.actions[self.menu.active]()	end,
		[2] = function()	self.menu.actions[self.menu.active]()	end,
		[3] = function()	self.menu.actions[self.menu.active]()	end,
		[4] = function()	self.menu.actions[self.menu.active]()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.double_esc()		end,
	}
	if switch[button] then switch[button]()	end
end
return mainmenu