local json = require("libs/dkjson")
local timer = require("libs/hump/timer")
local os_date = os.date
local load = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function load:init()
	self.activeColor = game.activeColor
	self.font = game.fonts["large"]
	self.timer = timer.new()
	self.sfx = game.sfx
end

function load:enter(state)
	about.scene = "load"	
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	self.quit = function()
		TEsound.stop("bgm")
		TEsound.play(self.sfx[1], "click", 0.6)	
		GS.switch(require("states/gamescreen"), true)
	end
	
	gooi.newButton("menu_btn", _,xc - (64 * sc + 12 * sc), 12 * sc, 64 * sc, 64 * sc, "graphics/static/gear.png", "load", sc)
	gooi.get("menu_btn"):onRelease(function() self.quit() end)
	
	self.menu = { buttons = {}, active = 1 }
	
	local savefile = {
		route2 = 0, 
		slots = { 
			[1] = { scene = "<empty>" },
			[2] = { scene = "<empty>" },
			[3] = { scene = "<empty>" },
			[4] = { scene = "<empty>" },
			[5] = { scene = "<empty>" },
		},
		achievements = { 
			img = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, }, 
			bgm = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0, [11] = 0, [12] = 0, },
			ed	= { [1] = 0, [2] = 0 },
		},
	}					
	if love.filesystem.exists("brm-save") then
		local data = love.filesystem.read("brm-save")
		savefile = json.decode(data)
	end
	
	self.action = function(id)	
		if savefile.slots[id]["scene"] ~= "<empty>" then
		print("load check", id,  savefile.slots[id]["scene"])
			about.session.cslot = savefile.slots[id]
			self.quit()			
		end		
	end
	
	local meta = {}
	for i = 1, #savefile.slots, 1 do
		meta[i] = {}
		meta[i]["label"] = savefile.slots[i]["scene"]
		if savefile.slots[i]["scene"] == "<empty>" then
			meta[i]["img"] = "locked"
		else
			meta[i]["label"] = meta[i]["label"].." - "..savefile.slots[i]["timestamp"]
			meta[i]["img"] = "icon_r"..savefile.slots[i]["route"]
		end
	end
	
	gooi.newPanel("load-grid", cx + 30 * sc, 110 * sc, (xc - cx) - 60 * sc, sH - 160 * sc, "grid 5x1")
	for i = 1, #meta, 1 do
		gooi.get("load-grid"):add(
			gooi.newButton("load-"..i, meta[i]["label"], _, _, _, _, "graphics/static/"..meta[i]["img"]..".png", "load", sc)
		)
		gooi.get("load-"..i).borderWidth = 5 * sc
		gooi.get("load-"..i):onRelease(function()	self.action(i)	end)
		table.insert(self.menu.buttons, "load-"..i)
	end
	
	self.onKeyDown = function()	
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor	
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor	
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		self.menu.actions[self.menu.active]()
	end
	
	self.onKeyStart = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= 1 then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = 1
		end
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[2], "switch", 0.6)
		if self.menu.active ~= #self.menu.buttons then
			gooi.get(self.menu.buttons[self.menu.active]).bgColor = game.style.bgColor
			self.menu.active = #self.menu.buttons
		end
	end
	
	self.joystickBlocked = false
	
end

function load:update(dt)
	
	gooi.get(self.menu.buttons[self.menu.active]).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			end
		end			
	end
	self.timer:update(dt)
	
end

function load:draw()
	love.graphics.printf(lang.game_options[2], 0, 30 * sc, sW, 'center')
	gooi.draw("load")
	drawFPS()	
end

function load:leave()
	self.timer:clear()
	gooi.remove("menu_btn")
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("load-"..i)
	end
	gooi.remove("load-grid")
end

function load:keypressed(key, unicode)

	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.action(self.menu.active)	end,
		[" "] = function()	self.action(self.menu.active)	end,
		["e"] = function()	self.action(self.menu.active)	end,
		["x"] = function()	self.action(self.menu.active)	end,
		["escape"] = function()	self.quit()	end,
	}
	if switch[key] then switch[key]()	end
	
end

function load:joystickpressed(joystick, button)
	
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
		
	local switch = {
		[1] = function()	self.action(self.menu.active)	end,
		[2] = function()	self.action(self.menu.active)	end,
		[3] = function()	self.action(self.menu.active)	end,
		[4] = function()	self.action(self.menu.active)	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	self.onKeyStart()	end,
		[11] = function()	self.quit()			end,
	}
	if switch[button] then switch[button]()	end
	
end
return load