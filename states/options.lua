local timer = require("libs/hump/timer")
local json = require("libs/dkjson")
local options = {}
local lang

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local drawFPS = game.drawFPS

function options:init()	
	self.sfx = game.sfx
	self.activeColor = game.activeColor	
end

function options:enter(state)
	lang = require("lang/"..about.settings.lang.."/"..about.settings.lang)
	about.scene = "options"
	
	if web == true then
		self.joystick = false
	else
		local joysticks = love.joystick.getJoysticks()
		self.joystick = joysticks[1]
	end
	
	self.menu = { sections = lang.options, labels = { [1] = { [1] = "English", [2] = "Русский" }, [2] = lang.switch }, radio = { [1] = true, [2] = false }, buttons = {}, actions = {}, active = 1 }
	self.fetch = { [1] = {}, [2] = {}, [3] = {} }
	local switch = {
		["en"] = function()	self.fetch[1][1] = true	self.fetch[1][2] = false	end,
		["template"] = function()	self.fetch[1][1] = true	self.fetch[1][2] = false	end,
		["ru"] = function()	self.fetch[1][1] = false	self.fetch[1][2] = true	end,
	}
	switch[about.settings.lang]()
	
	local delta = 70
	
	for i = 1, #self.menu.labels[1], 1 do	
		gooi.newRadio("options-"..i, self.menu.labels[1][i], sW / 2 - 130 * sc, delta * i * sc, 260 * sc, 60 * sc, self.fetch[1][i], "lang", "options-radio")
		table.insert(self.menu.buttons, "options-"..i)
	end
	
	for i = 1, #self.menu.labels[2], 1 do	
		gooi.newRadio("options-"..(i + 2), self.menu.labels[2][i], (sW / 5) * 2 - 170 * sc, (210 + delta * i) * sc, 245 * sc, 60 * sc, self.menu.radio[i], "bgm", "options-radio")
		table.insert(self.menu.buttons, "options-"..(i + 2))
	end
	
	for i = 1, #self.menu.labels[2], 1 do	
		gooi.newRadio("options-"..(i + 4) , self.menu.labels[2][i], (sW / 5) * 3 - 90 * sc, (210 + delta * i) * sc, 245 * sc, 60 * sc, self.menu.radio[i], "sfx", "options-radio")
		table.insert(self.menu.buttons, "options-"..(i + 4))
	end
	
	gooi.newSlider("options-7", sW / 2 - 130 * sc, (445 + delta) * sc, 260 * sc, 60 * sc, 1 - about.settings.textspd * 20)
	table.insert(self.menu.buttons, "options-slider")
	
	gooi.newButton("options-8", lang.back, sW / 2 - 130 * sc, 620 * sc, 260 * sc, 60 * sc)
	gooi.get("options-8").howRound = 1
	gooi.get("options-8"):generateBorder()
	table.insert(self.menu.buttons, "options-back")
	
	self.menu.actions[1] = function()
		local config = { settings = about.settings }
		love.filesystem.write("brm-settings.cfg", json.encode(config))
		GS.switch(require("states/mainmenu"))
	end
	
	self.onKeyDown = function()	
		gooi.get("options-"..self.menu.active).bgColor = game.style.bgColor		
		if self.menu.active + 1 <= #self.menu.buttons then			
			self.menu.active = self.menu.active + 1
		else
			self.menu.active = 1			
		end		
	end
	
	self.onKeyUp = function()
		gooi.get("options-"..self.menu.active).bgColor = game.style.bgColor		
		if self.menu.active - 1 >= 1 then
			self.menu.active = self.menu.active - 1	
		else
			self.menu.active = #self.menu.buttons		
		end	
	end
	
	self.onKeyUse = function()
		TEsound.play(self.sfx[1], "click", 0.6)
		
		if self.menu.active < #self.menu.buttons then
			gooi.get("options-"..self.menu.active):select()
		else
			self.menu.actions[1]()
		end
		
	end
	
	self.onKeyBack = function()
		TEsound.play(self.sfx[1], "click", 0.6)
		GS.switch(require("states/mainmenu"))
	end
	
	gooi.get("options-8"):onRelease(function(c) TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]() end)
	gooi.get("options-3").selected = about.settings.sound.bgm
	gooi.get("options-5").selected = about.settings.sound.sfx
	
	self.timer = timer.new()
	self.joystickBlocked = false	
end

function options:leave()
	for i = 1, #self.menu.buttons, 1 do
		gooi.remove("options-"..i)
	end
	self.timer:clear()
end

function options:update(dt)	
	gooi.get("options-"..self.menu.active).bgColor = self.activeColor
	gooi.update(dt)
	
	local function joystickHandler()	
		self.joystickBlocked = true
		TEsound.play(self.sfx[2], "switch", 0.6)
		self.blockTimer = self.timer:add(0.5, function() self.timer:cancel(self.blockTimer)	self.joystickBlocked = false end)		
	end
	
	if self.joystickBlocked == false then
		if self.joystick then
			if self.joystick:isGamepadDown('dpup') then
				joystickHandler()
				self.onKeyUp()
			elseif self.joystick:isGamepadDown('dpdown')	then
				joystickHandler()
				self.onKeyDown()
			elseif self.joystick:isGamepadDown('dpleft')	then
				joystickHandler()
				if self.menu.active == 7 then
					gooi.get("options-7").value = gooi.get("options-7").value - 0.15
					if gooi.get("options-7").value > 0.15 then gooi.get("options-7").value = 0.15	end
				end
			elseif self.joystick:isGamepadDown('dpright')	then
				joystickHandler()
				if self.menu.active == 7 then
					gooi.get("options-7").value = gooi.get("options-7").value + 0.15
					if gooi.get("options-7").value > 1 then gooi.get("options-7").value = 1	end
				end
			end
		end
		
	end
	
	if gooi.get("options-1").selected then
		about.settings.lang = "en"
	else
		if web then
			about.settings.lang = "en"
		else
			about.settings.lang = "ru"
		end
	end
	
	about.settings.sound.bgm = gooi.get("options-3").selected
	about.settings.sound.sfx = gooi.get("options-5").selected
	about.settings.textspd = (1 - gooi.get("options-7").value)/20
	
	if about.settings.sound.bgm == false then
		TEsound.stop("bgm", false)
	end
	
	self.timer:update(dt)
	
end

function options:draw()
	love.graphics.printf(lang.options[1], 0, 10 * sc, sW, 'center')
	love.graphics.printf(lang.options[2].."/"..lang.options[3], 0, 220 * sc, sW, 'center')
	love.graphics.printf(lang.options[4], 0, 450 * sc, sW, 'center')
	gooi.draw()
	if about.settings.gpswitch then
		gooi.draw("gamepad")
	end
	gooi.draw("options-radio")	
	drawFPS()
end

function options:keypressed(key, unicode)
	local switch = {
		["up"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["down"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["left"] = function()
				if self.menu.active == 7 then
					TEsound.play(self.sfx[1], "click", 0.6)
					gooi.get("options-7").value = gooi.get("options-7").value - 0.15
					if gooi.get("options-7").value > 0.15 then gooi.get("options-7").value = 0.15	end
				end
		end,
		["right"] = function()
				if self.menu.active == 7 then
					TEsound.play(self.sfx[1], "click", 0.6)
					gooi.get("options-7").value = gooi.get("options-7").value + 0.15
					if gooi.get("options-7").value > 1 then gooi.get("options-7").value = 1	end
				end
		end,
		["w"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyUp()	end,
		["s"] = function()	TEsound.play(self.sfx[2], "switch", 0.6)	self.onKeyDown()	end,
		["return"] = function()	self.onKeyUse()	end,
		[" "] = function()	self.onKeyUse()	end,
		["e"] = function()	self.onKeyUse()	end,
		["x"] = function()	self.onKeyUse()	end,
		["escape"] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]()	end,
		["menu"] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]()	end,
	}
	if switch[key] then switch[key]()	end
end

function options:joystickpressed(joystick, button)
	if	button == 5 or  button == 6	then	TEsound.play(self.sfx[2], "switch", 0.6)		end
	local switch = {
		[1] = function()	self.onKeyUse()	end,
		[2] = function()	self.onKeyUse()	end,
		[3] = function()	self.onKeyUse()	end,
		[4] = function()	self.onKeyUse()	end,
		[5] = function()	self.onKeyUp()	end,
		[6] = function()	self.onKeyDown()	end,
		[7] = function()	self.onKeyBack()	end,
		[8] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]()	end,
		[11] = function()	TEsound.play(self.sfx[1], "click", 0.6)	self.menu.actions[1]()	end
	}
	if switch[button] then switch[button]()	end	
end
return options	