GS = require("libs/hump/gamestate")
game = require("src/game")
require ("libs/tesound/TEsound")
require("libs/lovedebug")
if web == true then
	require("libs/gooi/init")
	slect_state = require("select_state")
else
	require("libs/gooi")
end

local sW, sH, sc = game.sW, game.sH, game.sc
local cx, cy, xc, yc = game.cx, game.cy, game.xc, game.yc
local background = game.background

_G.about = {
	ver = "1.2",
	type = 1,	--// extra stuff enabled
	scene = "main",
	settings = {
		sound = { sfx = true, bgm = true },
		lang = "en",
		gpswitch = true,
		textspd = 0.02,
	},
	session = {
		cslot = {
			shadow = {},	--// shadow image
		},			--// current slot	
	},
}

local sW, sH = love.graphics.getWidth(), love.graphics.getHeight()
gamestates = {}

function love.load()
	gooi.font = game.fonts["regular"]
	gooi.setStyle(game.style)
	if web == true then
		if inputHandler() ~= false then
			local function htmlCallback()
				if buffer() == 'q' then
					domapi.window.location='../main.html'
				end
				love.keypressed(buffer())
			end
			inputHandler(htmlCallback)
		end
		GS.registerEvents({ 'draw', 'errhand', 'update', 'mousepressed', 'mousereleased', 'touchpressed', 'touchreleased', 'keypressed', 'keyreleased', 'mousemoved', 'mousefocus', 'focus', 'visible', 'textinput' })
		GS.switch(select_state["intro"])
	else
		love.window.setIcon(love.image.newImageData("graphics/static/icon.png"))
		GS.registerEvents()
		GS.switch(require("states/intro"))
	end
end

function love.draw()
	
	if about.scene ~= "gamescreen" and about.scene ~= "intro" and about.scene ~= "credit_roll" then
		love.graphics.draw(background, cx, 0, _, sc, sc)
	end
	
end

function love.update(dt)
	TEsound.cleanup()
end

function love.mousepressed(x, y, button)  gooi.pressed() end
function love.mousereleased(x, y, button) gooi.released() end
function love.keypressed(key)   gooi.keypressed(key) end

function love.touchpressed(id, x, y, pressure)  gooi.pressed (id, x, y) end
function love.touchmoved(id, x, y, pressure)    gooi.moved   (id, x, y) end
function love.touchreleased(id, x, y, pressure) gooi.released(id, x, y) end